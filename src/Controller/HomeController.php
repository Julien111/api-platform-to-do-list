<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

final class HomeController extends AbstractController
{
    #[Route("/{reactRouting}", name: "tasks_home", requirements: ["reactRouting" => "^(?!api).+"], defaults: ["reactRouting" => null])]
     public function index(): Response
    {
        return $this->render('app/index.html.twig', []);
    }
}