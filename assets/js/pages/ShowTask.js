import React from 'react';
import { useState, useEffect } from "react";
import ListTasksAPI from "../services/listTasksAPI";
import { Link, useNavigate, useParams } from "react-router-dom";
import moment from 'moment';
import "../../styles/TasksList/tasksList.css";

export default function ShowTask() {

  const [task, setTask] = useState([]);
  const [loading, setLoading] = useState(false);   
  const [errorMessage, setErrorMessage] = useState("");
  const { id } = useParams();
  const navigate = useNavigate();


  const fetchTask = async () => {
    try {
      const response = await ListTasksAPI.find(id);
      setTask(response.data);
      setLoading(true);
    }
    catch (error) {
      console.error(error);
      setErrorMessage("Problème dans le chargement de la page.");
    }
  };

  const handleDelete = id => {
    try {
      ListTasksAPI.deleteTask(id);
      navigate('/list_tasks', { state: { message: 'La tâche a bien été supprimée.' } });
    }
    catch (error) {
      console.error(error);
      setErrorMessage("La tâche n'a pas pu être supprimé.");
    }
  }

  useEffect(() => {
    fetchTask();
  }, []);

  // connaitre le nombre de jour entre deux dates
  const handleDate = (date) => {
    const dateNow = moment().unix();
    console.log(dateNow);
    const newDate = moment(date, "YYYY-MM-DD").unix();
    console.log(newDate);
    const diff_temps = newDate - dateNow;
    let diff_jours = diff_temps / (1000 * 3600 * 24);
    return Math.round(diff_jours);
  }
  //fin test date


  return (
    <>
        <h2 className="title-show-task">Détail de la tâche</h2>
        <div>
        {errorMessage && (
          <p className="error"> {errorMessage} </p>
        )}
        {loading && (         
        
          <section className="container-task-only">
            <div key={task.id} className="card-task">
              <h2>{task.title}</h2>
              <div className="container-mark">
                <div><mark className={task.statut === 'En cours' ? "orange-item" : task.statut === "A faire" ? "red-item" : task.statut === "Terminé" ? "green-item" : "black-item"}>{task.statut}</mark></div>
              </div>
              <div>
                <p id="priorityElement">Priorité : {task.priority < 3 ? "Faible" : task.priority === 3 ? "Moyenne" : task.priority > 3 ? "Elevé" : ""}</p>
                <div id="dateEnd"><p>{handleDate(task.dateEnd) < 1 ? "La tâche doit être terminée !" : "Il reste : " + handleDate(task.dateEnd) + " jours"}</p></div>
              </div>
              <div className="container-btn-task">              
                
                <div className="container-edit-btn"><Link to={`/edit/${task.id}`} state={{ task }} >Modifier</Link></div>
                <button
                  onClick={() => handleDelete(task.id)}
                  className="btn-delete-task">
                  Supprimer
                </button>
              </div>   
            </div>   
          </section>
        )}

        </div>
        
    </>
    )
}
