import React from 'react';
import { useState, useEffect } from "react";
import ListTasksAPI from "../services/listTasksAPI";
import { useNavigate, useParams } from "react-router-dom";
import "../../styles/Forms/create.css";

export default function EditTask() {
  const [task, setTask] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const { id } = useParams();

  const fetchTask = async () => {
    try {
      const response = await ListTasksAPI.find(id);
      let dateEnd = response.data.dateEnd;
      let newDate = dateEnd.split("-").reverse().join("-");
      response.data.dateEnd = newDate;
      setTask(response.data);
    }
    catch (error) {
      console.error(error);
      setErrorMessage("La tâche n'a pas été trouvé.");
    }
  };

  useEffect(() => {
    fetchTask();
  }, []);

  //validation des données
  let message = "";
  let formMessage = document.querySelector('.form-message');

  const validateLength = (title, statut) => {
    if (title.length < 3 || statut.length < 3) {
      message = "Le titre ou le statut ne possède pas assez de caractères";
      return false;
    }
    else {
      return true;
    }
  };

  const isString = (title, statut) => {
    if (typeof title == "string" && typeof statut == "string") {
      if (title && statut) {
        return true;
      }
      else {
        message = "Le champ titre ou le statut est invalide";
        return false;
      }
    }
    else {
      message = "Le champ titre ou le champ statut est invalide";
      return false;
    }
  };

  const isNumber = (number) => {
    const regex = /^[1-5]$/;
    if (!regex.test(number) || !number) {
      message = "Le champ priorité doit être un nombre";
      return false;
    }
    else {
      //priority is a number so we need to replace the string by a number
      task["priority"] = parseInt(number);
      return true;
    }
  }

  const isDate = (date) => {

    if (typeof date == "string" && date) {
      const pattern = /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/;
      if (pattern.test(date)) {
        return true;
      }
      else {
        message = "Le format date n'est pas correct";
        return false;
      }
    }
    else {
      return false;
    }


  }
  // fin des validations

  // gestion des inputs

  const handleChange = ({ target }) => {
    const { name, value } = target;
    setTask({ ...task, [name]: value });
  };
  // fin gestion des inputs

  const handleSave = async event => {
    event.preventDefault();
    try {

      if (isString(task["title"], task["statut"]) && validateLength(task["title"], task["statut"]) && isDate(task["dateEnd"]) && isNumber(task["priority"])) {

        await ListTasksAPI.update(id, task);
        navigate('/list_tasks', { state: { message: 'La tâche a bien été modifiée.' } });
        setLoading(true);
      }
      else {
        formMessage.innerHTML = `<p>${message}.</p>`;
        formMessage.style.background = '#00c1ec';
        formMessage.style.opacity = '1';
        setLoading(false);
      }

    }
    catch (error) {
      console.error(error);
      setErrorMessage("La tâche n'a pas pu être modifié.");
    }
  };

  return (
    <>
      <h1 className="title-create-task">Modifier la tâche</h1>
      {errorMessage && (
        <p className="error"> {errorMessage} </p>
      )}
      <div className="form-message"></div>
      {!loading && (
        <section className="card-form-create">
          <div className="card-form-block">
            <form onSubmit={handleSave}>
              <div>
                <label htmlFor="title">Nom de la tâche</label>
                <input type="text" name="title" value={task.title || ''} onChange={handleChange} />
              </div>
              <div>
                <label htmlFor="statut">Statut</label>
                <select name="statut" value={task.statut} id="statut" onChange={handleChange}>
                  <option value="A faire">A faire</option>
                  <option value="En cours">En cours</option>
                  <option value="En attente">En attente</option>
                  <option value="Terminé">Terminé</option>
                </select>
              </div>
              <div>
                <label htmlFor="priority">Priorité : <em>(valeur entre 1 et 5)</em> </label>
                <input type="number" id="priority" name="priority" value={task.priority || ''} onChange={handleChange} min="1" max="5" />
              </div>
              <div id="block-dateEnd">
                <label htmlFor="dateEnd">Date de fin de la tache:</label>
                <input type="date" id="dateEndForm" name="dateEnd" value={task.dateEnd || ''} onChange={handleChange} />
              </div>         

              <div className="container-btn-submit"><button type="submit">Modifier</button></div>
            </form>
          </div>
        </section>
      )}
    </>
  )
}
