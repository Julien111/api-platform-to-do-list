import React from 'react';
import ListTasksAPI from "../services/listTasksAPI";
import { useState, useEffect } from "react";
import { Link, useLocation } from 'react-router-dom';
import moment from 'moment';
import "../../styles/TasksList/tasksList.css";

export default function TasksList() {

    const [tasks, setTasks] = useState([]);
    const [loading, setLoading] = useState(false);
    const location = useLocation();
    const [messageTrue, setMessageTrue] = useState(false);
    const [messageTest, setMessageTest] = useState("");
    const [errorMessage, setErrorMessage] = useState("");

    const fetchTasks = async () => {
        try {
            const response = await ListTasksAPI.findAll();
            setTasks(response);
            setLoading(true);
        }
        catch (error) {
            console.error(error);
            setErrorMessage("Problème de chargement de la page.");
        }
    };
    const handleDelete = async id => {
        try {
            setTasks(tasks.filter(task => task.id !== id));
            await ListTasksAPI.deleteTask(id);
        }
        catch (error) {
            console.error(error);
            setErrorMessage("La tâche n'a pas pu être supprimé.");
        }
    }

    useEffect(() => {
        //message flash
        if (location.state != null) {
            setMessageTrue(true);
            setMessageTest(location.state.message);
            window.history.replaceState({}, location.state.message);
        }
        else {
            setMessageTrue(false);
        }
        //affichage des données
        fetchTasks();
    }, []);

    // connaitre le nombre de jour entre deux dates
    const handleDate = (date) => {
        const dateNow = moment().unix();
        console.log(dateNow);
        const newDate = moment(date, "YYYY-MM-DD").unix();
        console.log(newDate);
        const diff_temps = newDate - dateNow;
        let diff_jours = diff_temps / (1000 * 3600 * 24);
        return Math.round(diff_jours);
    }

    //fin test date

    return (
        <>

            {loading && (

                <section className="page-tasksList">
                    <h1>Liste des tâches</h1>
                    {errorMessage && (
                        <p className="error"> {errorMessage} </p>
                    )}
                    <div className="messageFlash">
                        {messageTrue && (<p>{messageTest}</p>)}
                    </div>

                    <section className="container-tasks">
                        {tasks.map((task, index) => (
                            <div key={index} className="card-task">
                                <h2>{task.title}</h2>
                                <div className="container-mark">
                                    <div><mark className={task.statut === 'En cours' ? "orange-item" : task.statut === "A faire" ? "red-item" : task.statut === "Terminé" ? "green-item" : "black-item"}>{task.statut}</mark></div>
                                </div>
                                <div>
                                    <p id="priorityElement">Priorité : {task.priority < 3 ? "Faible" : task.priority === 3 ? "Moyenne" : task.priority > 3 ? "Elevé" : ""}</p>
                                    <div id="dateEnd"><p>{handleDate(task.dateEnd) < 1 ? "La tâche doit être terminée !" : "Il reste : " + handleDate(task.dateEnd) + " jours"}</p></div>
                                </div>
                                <div className="container-btn-task">
                                    <div className="container-detail-btn"><Link to={`/list_tasks/${task.id}`} state={{ task }} >En détail</Link>
                                    </div>
                                    <div className="container-edit-btn"><Link to={`/edit/${task.id}`} state={{ task }} >Modifier</Link></div>
                                    <button
                                        onClick={() => handleDelete(task.id)}
                                        className="btn-delete-task">
                                        Supprimer
                                    </button>
                                </div>
                            </div>
                        ))}
                    </section>
                </section>

            )}
        </>
    )
}
