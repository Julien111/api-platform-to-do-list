import React from 'react';
import "../../styles/Home/AboutUs.css";

export default function AboutUs() {
  return (
    <>
      <div className="page-aboutUs">
        <section className="container-aboutUs">
          <h1>&Agrave; propos</h1>
          <p>Réalisation d'une to-do list en Symfony et React js. Le but est d'apprendre à utiliser ces deux technologies avec Api Platform.</p>
        </section>
      </div>
    </>
  )
}
