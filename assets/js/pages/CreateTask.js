import { useState } from "react";
import { useNavigate } from 'react-router-dom';
import ListTasksAPI from "../services/listTasksAPI";
import Moment from 'moment';
import React from 'react';
import "../../styles/Forms/create.css";

export default function CreateTask() {

    const [loading, setLoading] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    const navigate = useNavigate();

    const [task, setTask] = useState({
        title: "",
        statut: "",
        createdAt: "",
        priority: "",
        dateEnd: ""
    });


    // gestion des inputs, on utilise le spread operator pour mettre l'objet complet et la value modifié
    const handleChange = ({ currentTarget }) => {
        const { name, value } = currentTarget;
        setTask({ ...task, [name]: value });
    };

    //validation des données
    let message = "";
    let formMessage = document.querySelector('.form-message');

    const validateLength = (title, statut) => {        
        if (title.length < 3 || statut.length < 3) {
            message = "Le titre ou le statut ne possède pas assez de caractères";
            return false;
        }
        else {
            return true;
        }
    };

    const isString = (title, statut) => {
        if (typeof title == "string" && typeof statut == "string") {
            if (title && statut) {
                return true;
            }
            else {
                message = "Le champ titre ou le statut est invalide";
                return false;
            }
        }
        else {
            message = "Le champ titre ou le champ statut est invalide";
            return false;
        }
    };

    const isNumber = (number) => {
        const regex = /^[1-5]$/;
        if (!regex.test(number) || !number) {
            message = "Le champ priorité doit être un nombre";
            return false;
        }
        else {
            //priority is a number so we need to replace the string by a number
            task["priority"] = parseInt(number);
            return true;
        }
    }

    const isDate = (date) => {

        if (typeof date == "string" && date) {
            const pattern = /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/;
            if (pattern.test(date)) {
                return true;
            }
            else {
                message = "Le format date n'est pas correct";
                return false;
            }
        }
        else {
            return false;
        }


    }
    // fin des validations

    // Gestion de la soumission du formulaire    

    const handleSubmit = async event => {
        event.preventDefault();
        try {
            //ajout date now
            const formatDate = Moment().format("YYYY-MM-DD H:m:s");
            task["createdAt"] = formatDate;
            //valider les datas dans la BDD

            if (isString(task["title"], task["statut"]) && validateLength(task["title"], task["statut"]) && isDate(task["dateEnd"]) && isNumber(task["priority"])) {

                await ListTasksAPI.create(task);
                navigate('/list_tasks', { state: { message: 'La tâche a bien été ajoutée' } });
            }
            else {
                formMessage.innerHTML = `<p>${message}.</p>`;
                formMessage.style.background = '#00c1ec';
                formMessage.style.opacity = '1';
                setLoading(false);
            }

        }
        catch (error) {
            console.log(error);
            setErrorMessage("Problème de chargement de la page.");
        }
    }

    return (
        <>
            <h1 className="title-create-task">Ajouter une tâche</h1>
            <div className="form-message"></div>
            {errorMessage && (
                <p className="error"> {errorMessage} </p>
            )}
            {!loading && (
                <section className="card-form-create">
                    <div className="card-form-block">
                        <form onSubmit={handleSubmit}>
                            <div>
                                <label htmlFor="title">Nom de la tâche : </label>
                                <input type="text" name="title" value={task.title} onChange={handleChange} />
                            </div>
                            <div>
                                <label htmlFor="statut">Statut : </label>
                                <select name="statut" value={task.statut} id="statut" onChange={handleChange}>
                                    <option value="">--Choisir un statut--</option>
                                    <option value="A faire">A faire</option>
                                    <option value="En cours">En cours</option>
                                    <option value="En attente">En attente</option>
                                </select>
                            </div>
                            <div>
                                <label htmlFor="priority">Priorité : <em>(valeur entre 1 et 5)</em></label>
                                <input type="number" id="priority" name="priority" value={task.priority} onChange={handleChange}
                                    min="1" max="5" />
                            </div>
                            <div id="block-dateEnd">
                                <label htmlFor="dateEnd">Date de fin de la tache : </label>
                                <input type="date" id="dateEndForm" name="dateEnd" value={task.dateEnd} onChange={handleChange} />
                            </div>

                            <div className="container-btn-submit"><button type="submit">Ajouter</button></div>
                        </form>
                    </div>
                </section>
            )}
        </>
    )
}
