import React from 'react';
import { Link } from 'react-router-dom';
import "../../styles/Home/Home.css";

export default function HomePage() {
    return (
        <section className="container-page-accueil">
            <h1>To-Do list</h1>

            <div className="part-text">
                <p>Une to-do list, ou liste de tâches, est un procédé qui se veut simple et efficace pour gérer les tâches d'un projet.</p>
            </div>

            <h3>Que souhaitez vous faire ?</h3>

            <div id="btn-List"><Link to="/list_tasks">Liste des tâches en cours</Link></div>

            <div id="btn-Create"><Link to="/create">Ajouter une tâche</Link></div>                   
        </section>
    )
}
