import React from 'react';
import HomePage from "./pages/HomePage";
import TasksList from "./pages/TasksList";
import CreateTask from "./pages/CreateTask";
import ShowTask from "./pages/ShowTask";
import EditTask from "./pages/EditTask";
import AboutUs from "./pages/AboutUs";
import Navbar from "./components/Navbar.js";
import "../styles/app.css";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

export default function App() {
    return (

        <Router>
            <Navbar />
            <main id="pages-app">
                <Routes>
                    <Route path="/" element={<HomePage />} />
                    <Route exact path="/list_tasks" element={<TasksList />} />
                    <Route path="/create" element={<CreateTask />} />
                    <Route path="/about" element={<AboutUs />} />
                    <Route path="/list_tasks/:id" element={<ShowTask />} />
                    <Route path="/edit/:id" element={<EditTask />} />
                </Routes>
            </main>
        </Router>

    )
}
