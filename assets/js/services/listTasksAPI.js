import axios from "axios";
import { LISTTASKS_API } from "../config.js";

async function findAll() {
    const response = await axios.get(LISTTASKS_API);
    const tasks = response.data["hydra:member"];
    return tasks;
}

async function create(task) {
    return await axios.post(LISTTASKS_API, task).then(function (response) {
        return response;
    })
        .catch(function (error) {
            console.log(error);
        });
}

async function find(id) {
    return axios.get(LISTTASKS_API + "/" + id).then(response => {
        return response;
    })
        .catch(function (error) {
            console.log(error);
        });
}

function update(id, task) {
    return axios.put(LISTTASKS_API + "/" + id, task).then(async response => {
        return response;
    })
        .catch(function (error) {
            console.log(error);
        });
}

function deleteTask(id){
    return axios.delete(LISTTASKS_API + "/" + id).then(async response => {
        return response;
    })
        .catch(function (error) {
            console.log(error);
        });
}

export default {
    findAll,
    create,
    update,
    find,
    deleteTask,
};


