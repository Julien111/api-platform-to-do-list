import React from 'react';
import "../../styles/Navbar/navbar.css";
import { Link } from 'react-router-dom';


export default function Navbar() {
    return (
        <nav>
            <ul>
                <li><Link to={`/`}>Accueil</Link></li>
                <li><Link to={`/list_tasks/`}>Les tâches</Link></li>
                <li><Link to={`/about`}>&Agrave; propos</Link></li>
            </ul>
        </nav>
    )
}
